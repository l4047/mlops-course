"""Utils for project."""

import inspect
from pathlib import Path
from typing import List


def to_abs_path(*args) -> List[str]:
    """Convert inputs to absolute path on disk.

    Returns:
        List[str]: absolute paths as text
    """
    _args = map(str, args)
    res = map(lambda x: Path(x).absolute(), _args)
    return list(map(str, res))


def self_name(stack_lvl: int = 1) -> str:
    """Return the name of the called function.

    Args:
        stack_lvl (int, optional): Stack depth to find function name. Defaults to 1.

    Returns:
        str: function name
    """
    stack = inspect.stack()[stack_lvl]
    module_name = Path(stack.filename).resolve().name
    return '{}.{}'.format(module_name.split('.')[0], stack.function)


if __name__ == "__main__":
    print(*to_abs_path("123", "abc"))
