"""Add new features."""
import click
import numpy as np
import pandas as pd

import src.utils.log as log
from src.utils.utils import to_abs_path


def add_features(in_path: str, out_path: str) -> None:
    """Add new features into input data set.

    Changes:
       - [+] `year` - extract year from `data` column
       - [+] `month` - extract month from `data` column
       - [-] `data` - dropped
       - [+] `level_to_levels` - 'level' / 'levels'
       - [+] `area_to_rooms` - ('area' / 'rooms').abs()
    Args:
        in_path (str): _description_
        out_path (str): _description_
    """
    logger = log.get_logger()
    logger.info("start to adding some new features")
    logger.debug("input file: %s", *to_abs_path(in_path))
    logger.debug("output file: %s", *to_abs_path(out_path))

    df = pd.read_csv(in_path)
    logger.debug(
        f"load dataset with (rows/cols) {df.index.size:,}/{df.columns.size}"
    )

    # Replace "date" with numeric features for year and month.
    df["date"] = pd.to_datetime(df["date"])
    df["year"] = df["date"].dt.year
    df["month"] = df["date"].dt.month
    df.drop("date", axis=1, inplace=True)
    # Apartment floor in relation to total number of floors.
    df["level_to_levels"] = df["level"] / df["levels"]
    # Average size of room in the apartment.
    df["area_to_rooms"] = (df["area"] / df["rooms"]).abs()
    # Fix division by zero.
    df.loc[df["area_to_rooms"] == np.inf, "area_to_rooms"] = df.loc[
        df["area_to_rooms"] == np.inf, "area"
    ]

    logger.info(f"output dataset with (rows/cols) {df.index.size:,}/{df.columns.size}")
    logger.debug("flushing data to disk")
    df.to_csv(out_path)
    logger.info("task compleat")


@click.command()
@click.argument("in_path", type=click.Path(exists=True, readable=True))
@click.argument("out_path", type=click.Path(exists=False))
@click.option(
    "--log",
    "-l",
    default="info",
    type=click.Choice(
        ["critical", "error", "warn", "warning", "info", "debug"], case_sensitive=False
    ),
    show_default=True,
)
def cli(**kwargs):
    """Add new features into input data set."""
    # extract and remove "log" from kwargs otherwise error in next call
    log.log_setup(kwargs.pop("log"))
    add_features(**kwargs)


if __name__ == "__main__":
    cli()
