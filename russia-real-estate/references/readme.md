
В рамках данного учебного проекта по MLOps выбран [Kaggle](https://www.kaggle.com) проект Russia-Real-Estimate:

- источник: <https://www.kaggle.com/datasets/mrdaniilak/russia-real-estate-20182021?datasetId=1340021>
- блокнот: [russian-housing-evaluation-model.ipynb](russian-housing-evaluation-model.ipynb)
