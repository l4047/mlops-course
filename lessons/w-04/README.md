# #4 Шаблонизация. Python пакеты и CLI. Snakemake. | MLOps и production подход в ML

## Материалы

### Cookiecutter

Cookiecutter создает проекты из шаблонов проектов, например проекты Python.

- Официальный сайт: <https://cookiecutter.readthedocs.io/en/latest/index.html>
- Установка: ```pip install cookiecutter```
- **Cookiecutter Data Science**
- Описание [Cookiecutter Data Science](https://drivendata.github.io/cookiecutter-data-science/)
    > **!!must read!!**  в данной статье описывается подход к работе с проектами DS, рекомендации по использованию различных инструментов
- GitHub <https://github.com/drivendata/cookiecutter-data-science>
- Старт нового проекта:
```cookiecutter https://github.com/drivendata/cookiecutter-data-science```

### Click

Click — это пакет Python для создания красивых интерфейсов командной строки компонуемым способом с минимальным количеством кода. Это «Комплект для создания интерфейса командной строки». Он легко настраивается, но поставляется с разумными настройками по умолчанию.

- документация: <https://click.palletsprojects.com/en/8.1.x/>
- установка:
    ```pip install click```

### Snakemake

Система управления рабочим процессом Snakemake — это инструмент для создания воспроизводимых и масштабируемых анализов данных. Рабочие процессы описываются с помощью удобочитаемого языка на основе Python. Их можно легко масштабировать для серверов, кластеров, сетевых и облачных сред без необходимости изменять определение рабочего процесса. Наконец, рабочие процессы Snakemake могут включать в себя описание необходимого программного обеспечения, которое будет автоматически развернуто в любой среде выполнения.

- офф. сайт: <https://snakemake.github.io/>
- документация: <https://snakemake.readthedocs.io/en/stable/>
- установка: из за сложных зависимостей, предлагается следующая последовательность
  - ```pip install Cython```
  - ```pip install wheel```
  - ```pip install git+https://github.com/snakemake/snakemake```
    > Если установка snakemake завершится ошибкой, скачиваем по ссылке в ошибке Microsoft Visual Studio 2022 (or later).и в установщике устанавливаем пакеты
    >
    > - Install the Python development workload and the optional Python native development tools option.
    > - Install the latest Windows SDK (under Native development in the installer).
    >
  - проверка установки, запустить команду ```snakemake```. В ответ будет сообщение о необходимости настройки кол-ва используемых ядер
    > Error: you need to specify the maximum number of CPU cores to be used at the same time. If you want to use N cores, say --cores N or -cN. For all cores on your system (be sure that this is appropriate) use --cores all. For no parallelization use --cores 1 or -c1.
  - установим использование всех ядер ```snake

## Шаги занятия

1. Зайти в корень вашего проекта
1. Установка [**Cookiecutter**](#cookiecutter)
    ```pip install cookiecutter```
    > установку сделал в глобальное окружение, что бы не повторять установку для каждого проекта отдельно. А так можно перед установкой создать venv, сделать установку в него, затем инициализировать структуру проекта и потом удалить venv.
1. Запустим скрипт для инициализации шаблона для проекта DS
    ```cookiecutter https://github.com/drivendata/cookiecutter-data-science```
    > после запуска скрипта необходимо ввести данные для инициализации
    - project_name [project_name]: `russia-real-estate`
    - repo_name [russia-real-estate]:
    - author_name [Your name (or your organization/company/team)]: `pro100v`
    - description [A short description of the project.]: `Test project within the MLOps course`
    - Select open_source_license: \
        1 - MIT \
        2 - BSD-3-Clause \
        3 - No license file \
        Choose from 1, 2, 3 [1]: `1`
    - s3_bucket [[OPTIONAL] your-bucket-for-syncing-data (do not include 's3://')]:
    - aws_profile [default]:
    - Select python_interpreter: \
        1 - python3 \
        2 - python \
        Choose from 1, 2 [1]: `1`
1. Проверим корректность настроенного окружения
    ```python C:\education\ods\mlops-course\russia-real-estate\test_environment.py```
    > `>>> Development environment passes all tests!`
1. Разместим в новой структуре наши данные.
1. В папку data/raw положим исходный файл проекта. Этот файл никогда не должен модифицироваться.
1. На основе исходного проекта [russia-real-estimation](../../russia-real-estate/references/russian-housing-evaluation-model.ipynb) создадим скрипты:
    - `def select_region` >> [`select_region.py`](../../russia-real-estate/src/data/select_region.py) в папке проекта [./src/data/](../../russia-real-estate/src/data/)
    - `def clean_data` >> [`clean_data.py`](../../russia-real-estate/src/data/clean_data.py) в папке проекта [./src/data/](../../russia-real-estate/src/data/)
    - `def add_features` >> [`add_features.py`](../../russia-real-estate/src/features/add_features.py) в папке проекта [./src/features/](../../russia-real-estate/src/features/)
    > для каждого модуля так же создается возможность его вызова через CLI, благодаря [Click](#click)

1. Создадим компоновочный файл [start.py](../../russia-real-estate/src/start.py) в котором последовательно обработаем все вызовы:

    ```python
    if __name__ == "__main__":
        log.log_setup('DEBUG')
        src.select_region(RAW_DATA_PATH, REGIONAL_DATA_PATH, REGION_ID)
        src.clean_data(
            REGIONAL_DATA_PATH,
            CLEANED_DATA_PATH,
            min_area=15,
            max_area=300,
            min_price=1_000_000,
            max_price=100_000_000,
            min_kitchen=3,
            max_kitchen=70,
        )
        src.add_features(CLEANED_DATA_PATH, FEATURED_DATA_PATH)
    ```

1. Кроме компоновочного файла, воспользуемся workflow менеджером [Snakemake](#snakemake). См. пункт установка в разделе [Snakemake](#snakemake)

   - в корне проекта создадим папку [workflow](../../venv/Lib/site-packages/snakemake/workflow.py)
   - в папке создадим файл [workflow/Snakefile](../../russia-real-estate/workflow/Snakefile)
   - в папке создадим файл [workflow/config.yaml](../../russia-real-estate/workflow/config.yaml)
   - после создания файла запустим линтер для проверки Snakefile: ```snakemake --lint```

1. Запустим workflow менеджером [Snakemake](#snakemake)
   - для холостого запуска ```snakemake -np```
        >
        > - n - холостой
        > - p - с выводом shell командами
        >
   - для запуска workflow ```snakemake --core all -p```

1. В качестве тестирования можно удалить один из промежуточных выходных файлов, запустить snakemake и убедится с какого шага начнет выполнятся DAG
