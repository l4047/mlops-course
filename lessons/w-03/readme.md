# #3 Codestyle, инструменты форматирования, линтеры

## Материалы

- [google drive](https://drive.google.com/drive/folders/15ueYiNJZnm5ohXdkwKofpg5CaBXvfUtT)
- Автоформатеры \
  ![Автоформатеры](autoformaters.png)
- Линтеры \
  ![Linters](linters.png)

## Шаги занятия

1. Создать репозиторий на GitLab
2. Инициализация репозитория в локальной папке
3. Связали локальный репозиторий с удаленным origin
4. Создадим файл gitignore
    - для удобства зайти в маркетплейс IDE и установить gitignore
    - добавить gitignore в индекс
5. Установка venv
    - зайти в папку проекта
    - выполнить команду `python -m venv ./venv`
    - активируем venv в cmd: `call venv/Scripts/activate`
6. Установка python autoformat - **Black**
    - пропущено т.к. интегрировано с VS Code (в настройках выбрать black в замен autopepe8)
    - страница проекта <https://black.readthedocs.io/en/stable/index.html>
7. Настройка ранера CI/CD в GitLab
    - Настройка ранеров
        - зайти в настройки settings > CI/CD > Runners
        - отключить ранеры по умолчанию, выключить `Enable shared runners for this project`
        - перейти при необходимости на страницу инструкции [Install GitLab Runner and ensure it's running](https://docs.gitlab.com/runner/install/)
        - установим ранеры на локальной машине
            - скачать файл установщика в зависимости от ОС
            - переименовать файл в `gitlab-runner.exe`
        - зарегистрировать ранер
            - открыть cmd от имени администратора
            - выполняем команду `gitlab-runner.exe register`
            - вводим данные со страницы настройки CI/CD > Runners (через копирование в буфер)
                - Register the runner with this URL: <https://gitlab.com/>
                - Enter the registration token: <REGISTRATION_TOKEN>
            - вводим доп. данные про ранер
                - описание (понятным языком, например: MLOps course runner)
                - добавляем через запятую теги (например: mlops, course)
                - добавляем описание для поддержки (например: This runner for education objects only (MLOps course))
                - вводим способ запуска (на проде желательно через docker, для быстроты и обучения выбираем shell)
            - после регистрации появится конфиг файл `config.toml`
                - меняем значения
                    - `concurrent = 4` - кол-во параллельно выполняющихся ранеров
                    - `shell = "powershell"` - команда вызова PowerShell
        - запускаем установку ранера на локальной машине `gitlab-runner.exe install`
        - запускаем ранер в качестве сервиса `gitlab-runner.exe start`
        - для проверки статуса можно выполнить `gitlab-runner.exe status`
    - После установки и регистрации ранера на локальной машине обновить страницу настроек GitLab, в разделе Runners должен появиться наш ранер
    - при необходимости можно изменить настройки через "карандаш"
        - установим флаг `Indicates whether this runner can pick jobs without tags`
8. Создадим CI/CD конфигурацию
   - перейдем в раздел CI/CD > Editor и жмем на кнопку `Configure pipeline`
   - откроется файл настройки пайплайна. Нажмем кнопку `Browse templates` для выбора python шаблона
   - откроем файл python шаблона и скопируем его содержимое в буфер
   - перейдем в наш файл с пайплайном и вставим в него шаблон python из буфера
   - немного про настройки
     - `image` указывает на версию python, можно указать конкретную версию, например 3.9
     - `cache` используется для кеширования папок, что бы снижать нагрузку на синхронизацию редко меняющихся данных, например библиотек в venv (если не указывать окружение будет каждый раз генерироваться заново)
     - `before_scrip` запуск скриптов до основного обработчика, что бы подготовить среду выполнения
     - `test`  запуск тестов наших скриптов
     - `run` запуск основного обработчика
     - `allow_failure: true` - этот параметр позволяет в любом случае сделать прохождение пайпа успешным. По умолчанию значение **false**

           ```yaml
           type_test:
               script:
                   - echo 'type_test pipeline'
                   - mypy --ignore-missing-imports ./russia-real-estate-20182021
               allow_failure: true
           ```

   - настраиваем наш пайплайн:
     - открываем меню CI/CD > Editor
     - выбираем нужную нам ветку
     - вставляем в редактор следующий блок:

      ```yaml
      # To contribute improvements to CI/CD templates, please follow the Development guide at:
      # https://docs.gitlab.com/ee/development/cicd/templates.html
      # This specific template is located at:
      # https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Python.gitlab-ci.yml

      # Official language image. Look for the different tagged releases at:
      # https://hub.docker.com/r/library/python/tags/
      image: python:latest

      # Change pip's cache directory to be inside the project directory since we can
      # only cache local items.
      variables:
      PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

      # Pip's cache doesn't store the python packages
      # https://pip.pypa.io/en/stable/topics/caching/
      #
      # If you want to also cache the installed packages, you have to install
      # them in a virtualenv and cache it as well.
      cache:
      paths:
          - .cache/pip
          - venv/

      before_script:
      - python --version
      - python3 -m venv ./venv
      - source venv/bin/activate
      - pip install -r requirements.txt
      # - Invoke-Expression ( "venv/Scripts/Activate.ps1" ) # for windows powershell

      type_test:
      script:
          - echo 'type_test pipeline'
          - mypy --ignore-missing-imports ./russia-real-estate-20182021
      # allow_failure: true # !!! allows the script to pass even if it failure

      lint_test:
      script:
          - flake8 ./russia-real-estate-20182021

      ```

     - применяем наши изменения. После коммита должен запуститься pipline, после некоторого времени должен отразиться результат его работы
9. Настроим конфигурацию для линтера
    - Создадим в корне проекта файл с названием `tox.ini`
    - Заполним его следующим содержимым

        ```ini
        [flake8]
        # https://www.flake8rules.com/
        # E203: Whitespace before ':'
        # H404: multi line docstring should start without a leading new line
        max-line-length = 88
        extended-ignore = E203
        ```
